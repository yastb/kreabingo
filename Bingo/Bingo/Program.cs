﻿
using Bingo.Classes;

#region input data

List<int> extraction = new List<int>();
extraction.AddRange(new int[] { 1, 76, 38, 96, 62, 41, 27, 33, 4, 2, 94, 15, 89, 25, 66, 14, 30, 0, 71, 21, 48, 44, 87, 73, 60, 
    50, 77, 45, 29, 18, 5, 99, 65, 16, 93, 95, 37, 3, 52, 32, 46, 80, 98, 63, 92, 24, 35, 55, 12, 81, 51, 17, 70, 78, 61, 91, 54, 
    8, 72, 40, 74, 68, 75, 67, 39, 64, 10, 53, 9, 31, 6, 7, 47, 42, 90, 20, 19, 36, 22, 43, 58, 28, 79, 86, 57, 49, 83, 84, 97, 
    11, 85, 26, 69, 23, 59, 82, 88, 34, 56, 13 }.ToList());

List<Board> boards = new List<Board>();

boards.Add(new Board(new BoardNumber[25]{new BoardNumber(85), new BoardNumber(23), new BoardNumber(65), new BoardNumber(78), new BoardNumber(93),
 new BoardNumber(27), new BoardNumber(53), new BoardNumber(10), new BoardNumber(12), new BoardNumber(26),
 new BoardNumber(5), new BoardNumber(34), new BoardNumber(83), new BoardNumber(25), new BoardNumber(6),
 new BoardNumber(56), new BoardNumber(40), new BoardNumber(73), new BoardNumber(29), new BoardNumber(54),
 new BoardNumber(33), new BoardNumber(68), new BoardNumber(41), new BoardNumber(32), new BoardNumber(82)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(8), new BoardNumber(31), new BoardNumber(14), new BoardNumber(70), new BoardNumber(91),
 new BoardNumber(53), new BoardNumber(49), new BoardNumber(86), new BoardNumber(13), new BoardNumber(21),
 new BoardNumber(66), new BoardNumber(28), new BoardNumber(76), new BoardNumber(78), new BoardNumber(93),
 new BoardNumber(39), new BoardNumber(63), new BoardNumber(80), new BoardNumber(43), new BoardNumber(23),
 new BoardNumber(56), new BoardNumber(25), new BoardNumber(60), new BoardNumber(67), new BoardNumber(72)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(67), new BoardNumber(78), new BoardNumber(36), new BoardNumber(64), new BoardNumber(14),
 new BoardNumber(46), new BoardNumber(16), new BoardNumber(80), new BoardNumber(23), new BoardNumber(94),
 new BoardNumber(22), new BoardNumber(47), new BoardNumber(51), new BoardNumber(65), new BoardNumber(57),
 new BoardNumber(33), new BoardNumber(76), new BoardNumber(21), new BoardNumber(92), new BoardNumber(97),
 new BoardNumber(31), new BoardNumber(95), new BoardNumber(54), new BoardNumber(27), new BoardNumber(20)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(1), new BoardNumber(77), new BoardNumber(86), new BoardNumber(43), new BoardNumber(30),
 new BoardNumber(28), new BoardNumber(88), new BoardNumber(7), new BoardNumber(5), new BoardNumber(60),
 new BoardNumber(66), new BoardNumber(24), new BoardNumber(3), new BoardNumber(57), new BoardNumber(33),
 new BoardNumber(38), new BoardNumber(23), new BoardNumber(59), new BoardNumber(84), new BoardNumber(44),
 new BoardNumber(74), new BoardNumber(47), new BoardNumber(17), new BoardNumber(29), new BoardNumber(85)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(21), new BoardNumber(50), new BoardNumber(86), new BoardNumber(2), new BoardNumber(70),
 new BoardNumber(85), new BoardNumber(19), new BoardNumber(22), new BoardNumber(93), new BoardNumber(25),
 new BoardNumber(99), new BoardNumber(38), new BoardNumber(74), new BoardNumber(30), new BoardNumber(65),
 new BoardNumber(81), new BoardNumber(0), new BoardNumber(47), new BoardNumber(78), new BoardNumber(63),
 new BoardNumber(34), new BoardNumber(11), new BoardNumber(51), new BoardNumber(88), new BoardNumber(64)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(45), new BoardNumber(15), new BoardNumber(29), new BoardNumber(81), new BoardNumber(30),
 new BoardNumber(75), new BoardNumber(21), new BoardNumber(88), new BoardNumber(91), new BoardNumber(49),
 new BoardNumber(39), new BoardNumber(20), new BoardNumber(4), new BoardNumber(17), new BoardNumber(78),
 new BoardNumber(10), new BoardNumber(12), new BoardNumber(38), new BoardNumber(11), new BoardNumber(7),
 new BoardNumber(98), new BoardNumber(6), new BoardNumber(65), new BoardNumber(69), new BoardNumber(86)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(36), new BoardNumber(20), new BoardNumber(31), new BoardNumber(44), new BoardNumber(69),
 new BoardNumber(30), new BoardNumber(65), new BoardNumber(55), new BoardNumber(88), new BoardNumber(64),
 new BoardNumber(74), new BoardNumber(85), new BoardNumber(82), new BoardNumber(61), new BoardNumber(5),
 new BoardNumber(57), new BoardNumber(17), new BoardNumber(90), new BoardNumber(43), new BoardNumber(54),
 new BoardNumber(58), new BoardNumber(83), new BoardNumber(52), new BoardNumber(23), new BoardNumber(7)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(42), new BoardNumber(16), new BoardNumber(82), new BoardNumber(86), new BoardNumber(76),
 new BoardNumber(60), new BoardNumber(26), new BoardNumber(27), new BoardNumber(59), new BoardNumber(55),
 new BoardNumber(7), new BoardNumber(53), new BoardNumber(22), new BoardNumber(78), new BoardNumber(5),
 new BoardNumber(18), new BoardNumber(61), new BoardNumber(10), new BoardNumber(15), new BoardNumber(17),
 new BoardNumber(28), new BoardNumber(46), new BoardNumber(14), new BoardNumber(87), new BoardNumber(77)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(21), new BoardNumber(43), new BoardNumber(15), new BoardNumber(47), new BoardNumber(61),
 new BoardNumber(24), new BoardNumber(76), new BoardNumber(28), new BoardNumber(3), new BoardNumber(27),
 new BoardNumber(19), new BoardNumber(62), new BoardNumber(69), new BoardNumber(82), new BoardNumber(93),
 new BoardNumber(49), new BoardNumber(29), new BoardNumber(97), new BoardNumber(74), new BoardNumber(41),
 new BoardNumber(92), new BoardNumber(36), new BoardNumber(37), new BoardNumber(99), new BoardNumber(40)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(31), new BoardNumber(4), new BoardNumber(3), new BoardNumber(62), new BoardNumber(51),
 new BoardNumber(24), new BoardNumber(57), new BoardNumber(78), new BoardNumber(67), new BoardNumber(53),
 new BoardNumber(13), new BoardNumber(5), new BoardNumber(76), new BoardNumber(38), new BoardNumber(55),
 new BoardNumber(79), new BoardNumber(9), new BoardNumber(75), new BoardNumber(98), new BoardNumber(71),
 new BoardNumber(65), new BoardNumber(1), new BoardNumber(39), new BoardNumber(18), new BoardNumber(47)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(59), new BoardNumber(4), new BoardNumber(38), new BoardNumber(95), new BoardNumber(99),
 new BoardNumber(85), new BoardNumber(68), new BoardNumber(69), new BoardNumber(93), new BoardNumber(43),
 new BoardNumber(83), new BoardNumber(57), new BoardNumber(48), new BoardNumber(42), new BoardNumber(15),
 new BoardNumber(47), new BoardNumber(50), new BoardNumber(80), new BoardNumber(79), new BoardNumber(90),
 new BoardNumber(56), new BoardNumber(87), new BoardNumber(78), new BoardNumber(64), new BoardNumber(25)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(21), new BoardNumber(37), new BoardNumber(14), new BoardNumber(67), new BoardNumber(95),
 new BoardNumber(88), new BoardNumber(39), new BoardNumber(26), new BoardNumber(38), new BoardNumber(49),
 new BoardNumber(89), new BoardNumber(83), new BoardNumber(54), new BoardNumber(77), new BoardNumber(96),
 new BoardNumber(48), new BoardNumber(86), new BoardNumber(94), new BoardNumber(19), new BoardNumber(20),
 new BoardNumber(43), new BoardNumber(41), new BoardNumber(8), new BoardNumber(74), new BoardNumber(58)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(1), new BoardNumber(36), new BoardNumber(12), new BoardNumber(90), new BoardNumber(91),
 new BoardNumber(63), new BoardNumber(21), new BoardNumber(98), new BoardNumber(82), new BoardNumber(66),
 new BoardNumber(39), new BoardNumber(86), new BoardNumber(7), new BoardNumber(52), new BoardNumber(77),
 new BoardNumber(80), new BoardNumber(81), new BoardNumber(44), new BoardNumber(33), new BoardNumber(58),
 new BoardNumber(78), new BoardNumber(30), new BoardNumber(11), new BoardNumber(51), new BoardNumber(28)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(81), new BoardNumber(74), new BoardNumber(7), new BoardNumber(33), new BoardNumber(96),
 new BoardNumber(75), new BoardNumber(60), new BoardNumber(87), new BoardNumber(47), new BoardNumber(91),
 new BoardNumber(39), new BoardNumber(73), new BoardNumber(30), new BoardNumber(50), new BoardNumber(13),
 new BoardNumber(4), new BoardNumber(41), new BoardNumber(9), new BoardNumber(43), new BoardNumber(77),
 new BoardNumber(34), new BoardNumber(82), new BoardNumber(72), new BoardNumber(48), new BoardNumber(12)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(93), new BoardNumber(63), new BoardNumber(74), new BoardNumber(25), new BoardNumber(57),
 new BoardNumber(29), new BoardNumber(76), new BoardNumber(9), new BoardNumber(45), new BoardNumber(70),
 new BoardNumber(98), new BoardNumber(77), new BoardNumber(71), new BoardNumber(16), new BoardNumber(41),
 new BoardNumber(47), new BoardNumber(54), new BoardNumber(18), new BoardNumber(14), new BoardNumber(55),
 new BoardNumber(31), new BoardNumber(89), new BoardNumber(67), new BoardNumber(87), new BoardNumber(83)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(8), new BoardNumber(72), new BoardNumber(45), new BoardNumber(93), new BoardNumber(68),
 new BoardNumber(74), new BoardNumber(26), new BoardNumber(69), new BoardNumber(94), new BoardNumber(65),
 new BoardNumber(28), new BoardNumber(9), new BoardNumber(20), new BoardNumber(47), new BoardNumber(41),
 new BoardNumber(46), new BoardNumber(54), new BoardNumber(21), new BoardNumber(56), new BoardNumber(22),
 new BoardNumber(84), new BoardNumber(62), new BoardNumber(18), new BoardNumber(15), new BoardNumber(48)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(20), new BoardNumber(51), new BoardNumber(81), new BoardNumber(40), new BoardNumber(69),
 new BoardNumber(71), new BoardNumber(10), new BoardNumber(13), new BoardNumber(93), new BoardNumber(75),
 new BoardNumber(44), new BoardNumber(86), new BoardNumber(0), new BoardNumber(95), new BoardNumber(37),
 new BoardNumber(99), new BoardNumber(39), new BoardNumber(76), new BoardNumber(80), new BoardNumber(66),
 new BoardNumber(14), new BoardNumber(64), new BoardNumber(49), new BoardNumber(62), new BoardNumber(27)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(75), new BoardNumber(7), new BoardNumber(51), new BoardNumber(86), new BoardNumber(79),
 new BoardNumber(43), new BoardNumber(30), new BoardNumber(61), new BoardNumber(39), new BoardNumber(16),
 new BoardNumber(85), new BoardNumber(63), new BoardNumber(90), new BoardNumber(28), new BoardNumber(96),
 new BoardNumber(88), new BoardNumber(78), new BoardNumber(72), new BoardNumber(31), new BoardNumber(73),
 new BoardNumber(98), new BoardNumber(87), new BoardNumber(23), new BoardNumber(19), new BoardNumber(58)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(20), new BoardNumber(95), new BoardNumber(47), new BoardNumber(97), new BoardNumber(12),
 new BoardNumber(92), new BoardNumber(25), new BoardNumber(68), new BoardNumber(87), new BoardNumber(91),
 new BoardNumber(37), new BoardNumber(10), new BoardNumber(78), new BoardNumber(23), new BoardNumber(63),
 new BoardNumber(74), new BoardNumber(93), new BoardNumber(58), new BoardNumber(39), new BoardNumber(5),
 new BoardNumber(76), new BoardNumber(51), new BoardNumber(48), new BoardNumber(72), new BoardNumber(16)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(37), new BoardNumber(18), new BoardNumber(32), new BoardNumber(34), new BoardNumber(85),
 new BoardNumber(22), new BoardNumber(31), new BoardNumber(98), new BoardNumber(42), new BoardNumber(19),
 new BoardNumber(29), new BoardNumber(72), new BoardNumber(48), new BoardNumber(76), new BoardNumber(25),
 new BoardNumber(47), new BoardNumber(1), new BoardNumber(21), new BoardNumber(7), new BoardNumber(53),
 new BoardNumber(79), new BoardNumber(82), new BoardNumber(86), new BoardNumber(52), new BoardNumber(78)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(20), new BoardNumber(16), new BoardNumber(47), new BoardNumber(78), new BoardNumber(92),
 new BoardNumber(88), new BoardNumber(15), new BoardNumber(71), new BoardNumber(67), new BoardNumber(2),
 new BoardNumber(5), new BoardNumber(52), new BoardNumber(90), new BoardNumber(70), new BoardNumber(9),
 new BoardNumber(22), new BoardNumber(49), new BoardNumber(28), new BoardNumber(82), new BoardNumber(27),
 new BoardNumber(6), new BoardNumber(19), new BoardNumber(61), new BoardNumber(73), new BoardNumber(48)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(71), new BoardNumber(26), new BoardNumber(7), new BoardNumber(11), new BoardNumber(79),
 new BoardNumber(52), new BoardNumber(30), new BoardNumber(47), new BoardNumber(1), new BoardNumber(31),
 new BoardNumber(17), new BoardNumber(75), new BoardNumber(94), new BoardNumber(91), new BoardNumber(28),
 new BoardNumber(81), new BoardNumber(98), new BoardNumber(23), new BoardNumber(55), new BoardNumber(21),
 new BoardNumber(77), new BoardNumber(15), new BoardNumber(39), new BoardNumber(24), new BoardNumber(16)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(5), new BoardNumber(75), new BoardNumber(44), new BoardNumber(88), new BoardNumber(65),
 new BoardNumber(89), new BoardNumber(45), new BoardNumber(23), new BoardNumber(69), new BoardNumber(19),
 new BoardNumber(41), new BoardNumber(61), new BoardNumber(67), new BoardNumber(52), new BoardNumber(54),
 new BoardNumber(47), new BoardNumber(38), new BoardNumber(57), new BoardNumber(12), new BoardNumber(98),
 new BoardNumber(62), new BoardNumber(70), new BoardNumber(26), new BoardNumber(87), new BoardNumber(53)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(50), new BoardNumber(4), new BoardNumber(65), new BoardNumber(77), new BoardNumber(25),
 new BoardNumber(6), new BoardNumber(21), new BoardNumber(5), new BoardNumber(27), new BoardNumber(92),
 new BoardNumber(39), new BoardNumber(63), new BoardNumber(97), new BoardNumber(75), new BoardNumber(79),
 new BoardNumber(60), new BoardNumber(34), new BoardNumber(87), new BoardNumber(26), new BoardNumber(74),
 new BoardNumber(99), new BoardNumber(24), new BoardNumber(44), new BoardNumber(85), new BoardNumber(2)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(13), new BoardNumber(64), new BoardNumber(38), new BoardNumber(78), new BoardNumber(21),
 new BoardNumber(74), new BoardNumber(17), new BoardNumber(83), new BoardNumber(57), new BoardNumber(94),
 new BoardNumber(25), new BoardNumber(39), new BoardNumber(69), new BoardNumber(53), new BoardNumber(4),
 new BoardNumber(54), new BoardNumber(33), new BoardNumber(81), new BoardNumber(50), new BoardNumber(76),
 new BoardNumber(42), new BoardNumber(75), new BoardNumber(19), new BoardNumber(77), new BoardNumber(26)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(63), new BoardNumber(31), new BoardNumber(70), new BoardNumber(19), new BoardNumber(39),
 new BoardNumber(38), new BoardNumber(87), new BoardNumber(15), new BoardNumber(90), new BoardNumber(75),
 new BoardNumber(61), new BoardNumber(98), new BoardNumber(6), new BoardNumber(29), new BoardNumber(86),
 new BoardNumber(78), new BoardNumber(62), new BoardNumber(32), new BoardNumber(11), new BoardNumber(60),
 new BoardNumber(55), new BoardNumber(97), new BoardNumber(13), new BoardNumber(73), new BoardNumber(82)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(51), new BoardNumber(63), new BoardNumber(68), new BoardNumber(84), new BoardNumber(36),
 new BoardNumber(12), new BoardNumber(33), new BoardNumber(37), new BoardNumber(31), new BoardNumber(8),
 new BoardNumber(18), new BoardNumber(41), new BoardNumber(34), new BoardNumber(74), new BoardNumber(23),
 new BoardNumber(72), new BoardNumber(39), new BoardNumber(85), new BoardNumber(48), new BoardNumber(60),
 new BoardNumber(24), new BoardNumber(19), new BoardNumber(29), new BoardNumber(88), new BoardNumber(0)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(46), new BoardNumber(51), new BoardNumber(17), new BoardNumber(23), new BoardNumber(13),
 new BoardNumber(20), new BoardNumber(93), new BoardNumber(97), new BoardNumber(99), new BoardNumber(81),
 new BoardNumber(57), new BoardNumber(47), new BoardNumber(33), new BoardNumber(84), new BoardNumber(44),
 new BoardNumber(28), new BoardNumber(96), new BoardNumber(2), new BoardNumber(43), new BoardNumber(56),
 new BoardNumber(68), new BoardNumber(36), new BoardNumber(62), new BoardNumber(15), new BoardNumber(5)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(81), new BoardNumber(99), new BoardNumber(5), new BoardNumber(30), new BoardNumber(10),
 new BoardNumber(38), new BoardNumber(62), new BoardNumber(57), new BoardNumber(8), new BoardNumber(37),
 new BoardNumber(7), new BoardNumber(86), new BoardNumber(98), new BoardNumber(3), new BoardNumber(54),
 new BoardNumber(46), new BoardNumber(82), new BoardNumber(96), new BoardNumber(15), new BoardNumber(72),
 new BoardNumber(83), new BoardNumber(1), new BoardNumber(75), new BoardNumber(25), new BoardNumber(50)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(47), new BoardNumber(57), new BoardNumber(11), new BoardNumber(61), new BoardNumber(27),
 new BoardNumber(53), new BoardNumber(10), new BoardNumber(31), new BoardNumber(91), new BoardNumber(98),
 new BoardNumber(76), new BoardNumber(85), new BoardNumber(55), new BoardNumber(38), new BoardNumber(23),
 new BoardNumber(6), new BoardNumber(81), new BoardNumber(67), new BoardNumber(71), new BoardNumber(70),
 new BoardNumber(35), new BoardNumber(29), new BoardNumber(17), new BoardNumber(50), new BoardNumber(56)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(24), new BoardNumber(65), new BoardNumber(15), new BoardNumber(1), new BoardNumber(89),
 new BoardNumber(45), new BoardNumber(60), new BoardNumber(97), new BoardNumber(23), new BoardNumber(14),
 new BoardNumber(84), new BoardNumber(56), new BoardNumber(58), new BoardNumber(5), new BoardNumber(54),
 new BoardNumber(3), new BoardNumber(72), new BoardNumber(51), new BoardNumber(46), new BoardNumber(79),
 new BoardNumber(67), new BoardNumber(70), new BoardNumber(78), new BoardNumber(34), new BoardNumber(77)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(38), new BoardNumber(11), new BoardNumber(54), new BoardNumber(23), new BoardNumber(2),
 new BoardNumber(33), new BoardNumber(14), new BoardNumber(10), new BoardNumber(96), new BoardNumber(63),
 new BoardNumber(43), new BoardNumber(5), new BoardNumber(36), new BoardNumber(20), new BoardNumber(30),
 new BoardNumber(70), new BoardNumber(53), new BoardNumber(66), new BoardNumber(71), new BoardNumber(9),
 new BoardNumber(91), new BoardNumber(90), new BoardNumber(21), new BoardNumber(7), new BoardNumber(88)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(94), new BoardNumber(44), new BoardNumber(4), new BoardNumber(86), new BoardNumber(26),
 new BoardNumber(39), new BoardNumber(70), new BoardNumber(54), new BoardNumber(50), new BoardNumber(30),
 new BoardNumber(55), new BoardNumber(40), new BoardNumber(12), new BoardNumber(72), new BoardNumber(71),
 new BoardNumber(68), new BoardNumber(7), new BoardNumber(66), new BoardNumber(47), new BoardNumber(91),
 new BoardNumber(31), new BoardNumber(24), new BoardNumber(13), new BoardNumber(1), new BoardNumber(96)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(79), new BoardNumber(14), new BoardNumber(40), new BoardNumber(87), new BoardNumber(68),
 new BoardNumber(16), new BoardNumber(32), new BoardNumber(53), new BoardNumber(46), new BoardNumber(98),
 new BoardNumber(38), new BoardNumber(95), new BoardNumber(21), new BoardNumber(89), new BoardNumber(69),
 new BoardNumber(62), new BoardNumber(60), new BoardNumber(19), new BoardNumber(81), new BoardNumber(33),
 new BoardNumber(70), new BoardNumber(52), new BoardNumber(28), new BoardNumber(83), new BoardNumber(0)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(62), new BoardNumber(42), new BoardNumber(38), new BoardNumber(48), new BoardNumber(64),
 new BoardNumber(61), new BoardNumber(79), new BoardNumber(78), new BoardNumber(97), new BoardNumber(98),
 new BoardNumber(89), new BoardNumber(7), new BoardNumber(3), new BoardNumber(29), new BoardNumber(68),
 new BoardNumber(92), new BoardNumber(76), new BoardNumber(14), new BoardNumber(67), new BoardNumber(1),
 new BoardNumber(41), new BoardNumber(99), new BoardNumber(72), new BoardNumber(47), new BoardNumber(60)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(5), new BoardNumber(75), new BoardNumber(18), new BoardNumber(42), new BoardNumber(33),
 new BoardNumber(72), new BoardNumber(61), new BoardNumber(36), new BoardNumber(31), new BoardNumber(29),
 new BoardNumber(19), new BoardNumber(58), new BoardNumber(1), new BoardNumber(34), new BoardNumber(94),
 new BoardNumber(54), new BoardNumber(84), new BoardNumber(92), new BoardNumber(99), new BoardNumber(38),
 new BoardNumber(76), new BoardNumber(68), new BoardNumber(79), new BoardNumber(53), new BoardNumber(37)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(14), new BoardNumber(91), new BoardNumber(37), new BoardNumber(5), new BoardNumber(98),
 new BoardNumber(68), new BoardNumber(29), new BoardNumber(34), new BoardNumber(76), new BoardNumber(43),
 new BoardNumber(75), new BoardNumber(0), new BoardNumber(67), new BoardNumber(33), new BoardNumber(69),
 new BoardNumber(81), new BoardNumber(47), new BoardNumber(58), new BoardNumber(30), new BoardNumber(93),
 new BoardNumber(88), new BoardNumber(92), new BoardNumber(42), new BoardNumber(77), new BoardNumber(54)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(64), new BoardNumber(24), new BoardNumber(28), new BoardNumber(54), new BoardNumber(53),
 new BoardNumber(72), new BoardNumber(68), new BoardNumber(3), new BoardNumber(73), new BoardNumber(4),
 new BoardNumber(83), new BoardNumber(6), new BoardNumber(59), new BoardNumber(66), new BoardNumber(94),
 new BoardNumber(87), new BoardNumber(80), new BoardNumber(55), new BoardNumber(20), new BoardNumber(16),
 new BoardNumber(13), new BoardNumber(82), new BoardNumber(74), new BoardNumber(31), new BoardNumber(70)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(63), new BoardNumber(92), new BoardNumber(71), new BoardNumber(0), new BoardNumber(83),
 new BoardNumber(98), new BoardNumber(40), new BoardNumber(50), new BoardNumber(55), new BoardNumber(2),
 new BoardNumber(88), new BoardNumber(5), new BoardNumber(85), new BoardNumber(30), new BoardNumber(23),
 new BoardNumber(10), new BoardNumber(75), new BoardNumber(81), new BoardNumber(58), new BoardNumber(68),
 new BoardNumber(51), new BoardNumber(31), new BoardNumber(14), new BoardNumber(89), new BoardNumber(1)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(67), new BoardNumber(93), new BoardNumber(94), new BoardNumber(54), new BoardNumber(53),
 new BoardNumber(38), new BoardNumber(71), new BoardNumber(34), new BoardNumber(40), new BoardNumber(24),
 new BoardNumber(31), new BoardNumber(63), new BoardNumber(30), new BoardNumber(99), new BoardNumber(75),
 new BoardNumber(4), new BoardNumber(57), new BoardNumber(86), new BoardNumber(19), new BoardNumber(70),
 new BoardNumber(60), new BoardNumber(49), new BoardNumber(87), new BoardNumber(68), new BoardNumber(74)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(56), new BoardNumber(94), new BoardNumber(79), new BoardNumber(53), new BoardNumber(7),
 new BoardNumber(24), new BoardNumber(12), new BoardNumber(19), new BoardNumber(6), new BoardNumber(99),
 new BoardNumber(82), new BoardNumber(51), new BoardNumber(41), new BoardNumber(46), new BoardNumber(43),
 new BoardNumber(17), new BoardNumber(49), new BoardNumber(52), new BoardNumber(78), new BoardNumber(55),
 new BoardNumber(75), new BoardNumber(48), new BoardNumber(61), new BoardNumber(70), new BoardNumber(87)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(14), new BoardNumber(55), new BoardNumber(32), new BoardNumber(21), new BoardNumber(31),
 new BoardNumber(88), new BoardNumber(83), new BoardNumber(23), new BoardNumber(44), new BoardNumber(4),
 new BoardNumber(1), new BoardNumber(77), new BoardNumber(45), new BoardNumber(90), new BoardNumber(85),
 new BoardNumber(46), new BoardNumber(81), new BoardNumber(51), new BoardNumber(27), new BoardNumber(62),
 new BoardNumber(60), new BoardNumber(24), new BoardNumber(29), new BoardNumber(18), new BoardNumber(0)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(95), new BoardNumber(92), new BoardNumber(91), new BoardNumber(27), new BoardNumber(26),
 new BoardNumber(22), new BoardNumber(43), new BoardNumber(45), new BoardNumber(64), new BoardNumber(62),
 new BoardNumber(83), new BoardNumber(23), new BoardNumber(25), new BoardNumber(85), new BoardNumber(94),
 new BoardNumber(84), new BoardNumber(53), new BoardNumber(72), new BoardNumber(28), new BoardNumber(20),
 new BoardNumber(75), new BoardNumber(60), new BoardNumber(52), new BoardNumber(18), new BoardNumber(73)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(95), new BoardNumber(41), new BoardNumber(7), new BoardNumber(21), new BoardNumber(32),
 new BoardNumber(58), new BoardNumber(65), new BoardNumber(16), new BoardNumber(56), new BoardNumber(97),
 new BoardNumber(68), new BoardNumber(25), new BoardNumber(91), new BoardNumber(83), new BoardNumber(24),
 new BoardNumber(66), new BoardNumber(89), new BoardNumber(15), new BoardNumber(55), new BoardNumber(6),
 new BoardNumber(2), new BoardNumber(30), new BoardNumber(84), new BoardNumber(10), new BoardNumber(90)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(58), new BoardNumber(86), new BoardNumber(44), new BoardNumber(19), new BoardNumber(74),
 new BoardNumber(57), new BoardNumber(89), new BoardNumber(17), new BoardNumber(6), new BoardNumber(83),
 new BoardNumber(77), new BoardNumber(35), new BoardNumber(60), new BoardNumber(32), new BoardNumber(13),
 new BoardNumber(97), new BoardNumber(63), new BoardNumber(62), new BoardNumber(28), new BoardNumber(76),
 new BoardNumber(55), new BoardNumber(31), new BoardNumber(11), new BoardNumber(0), new BoardNumber(52)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(33), new BoardNumber(39), new BoardNumber(59), new BoardNumber(42), new BoardNumber(45),
 new BoardNumber(61), new BoardNumber(50), new BoardNumber(92), new BoardNumber(9), new BoardNumber(79),
 new BoardNumber(15), new BoardNumber(0), new BoardNumber(28), new BoardNumber(5), new BoardNumber(72),
 new BoardNumber(91), new BoardNumber(24), new BoardNumber(21), new BoardNumber(29), new BoardNumber(87),
 new BoardNumber(86), new BoardNumber(76), new BoardNumber(43), new BoardNumber(31), new BoardNumber(93)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(63), new BoardNumber(11), new BoardNumber(86), new BoardNumber(45), new BoardNumber(85),
 new BoardNumber(96), new BoardNumber(74), new BoardNumber(66), new BoardNumber(93), new BoardNumber(32),
 new BoardNumber(95), new BoardNumber(30), new BoardNumber(99), new BoardNumber(23), new BoardNumber(18),
 new BoardNumber(69), new BoardNumber(97), new BoardNumber(48), new BoardNumber(15), new BoardNumber(1),
 new BoardNumber(42), new BoardNumber(87), new BoardNumber(47), new BoardNumber(83), new BoardNumber(80)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(93), new BoardNumber(5), new BoardNumber(40), new BoardNumber(64), new BoardNumber(2),
 new BoardNumber(44), new BoardNumber(51), new BoardNumber(15), new BoardNumber(54), new BoardNumber(83),
 new BoardNumber(69), new BoardNumber(77), new BoardNumber(90), new BoardNumber(58), new BoardNumber(11),
 new BoardNumber(0), new BoardNumber(48), new BoardNumber(43), new BoardNumber(30), new BoardNumber(55),
 new BoardNumber(25), new BoardNumber(72), new BoardNumber(38), new BoardNumber(73), new BoardNumber(52)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(89), new BoardNumber(58), new BoardNumber(71), new BoardNumber(68), new BoardNumber(15),
 new BoardNumber(23), new BoardNumber(65), new BoardNumber(9), new BoardNumber(36), new BoardNumber(74),
 new BoardNumber(21), new BoardNumber(29), new BoardNumber(42), new BoardNumber(79), new BoardNumber(98),
 new BoardNumber(55), new BoardNumber(47), new BoardNumber(33), new BoardNumber(39), new BoardNumber(28),
 new BoardNumber(16), new BoardNumber(75), new BoardNumber(91), new BoardNumber(69), new BoardNumber(57)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(13), new BoardNumber(79), new BoardNumber(12), new BoardNumber(71), new BoardNumber(2),
 new BoardNumber(60), new BoardNumber(94), new BoardNumber(99), new BoardNumber(43), new BoardNumber(82),
 new BoardNumber(84), new BoardNumber(89), new BoardNumber(29), new BoardNumber(91), new BoardNumber(87),
 new BoardNumber(74), new BoardNumber(80), new BoardNumber(25), new BoardNumber(32), new BoardNumber(21),
 new BoardNumber(70), new BoardNumber(14), new BoardNumber(68), new BoardNumber(92), new BoardNumber(11)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(78), new BoardNumber(1), new BoardNumber(16), new BoardNumber(51), new BoardNumber(87),
 new BoardNumber(58), new BoardNumber(94), new BoardNumber(59), new BoardNumber(15), new BoardNumber(43),
 new BoardNumber(79), new BoardNumber(41), new BoardNumber(50), new BoardNumber(47), new BoardNumber(39),
 new BoardNumber(53), new BoardNumber(37), new BoardNumber(9), new BoardNumber(28), new BoardNumber(72),
 new BoardNumber(34), new BoardNumber(63), new BoardNumber(89), new BoardNumber(35), new BoardNumber(18)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(31), new BoardNumber(67), new BoardNumber(70), new BoardNumber(42), new BoardNumber(43),
 new BoardNumber(60), new BoardNumber(2), new BoardNumber(89), new BoardNumber(49), new BoardNumber(22),
 new BoardNumber(56), new BoardNumber(17), new BoardNumber(81), new BoardNumber(24), new BoardNumber(74),
 new BoardNumber(20), new BoardNumber(65), new BoardNumber(1), new BoardNumber(96), new BoardNumber(51),
 new BoardNumber(68), new BoardNumber(7), new BoardNumber(0), new BoardNumber(38), new BoardNumber(25)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(59), new BoardNumber(14), new BoardNumber(29), new BoardNumber(53), new BoardNumber(19),
 new BoardNumber(9), new BoardNumber(2), new BoardNumber(11), new BoardNumber(33), new BoardNumber(44),
 new BoardNumber(81), new BoardNumber(6), new BoardNumber(10), new BoardNumber(47), new BoardNumber(58),
 new BoardNumber(20), new BoardNumber(34), new BoardNumber(62), new BoardNumber(55), new BoardNumber(40),
 new BoardNumber(71), new BoardNumber(38), new BoardNumber(69), new BoardNumber(45), new BoardNumber(78)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(59), new BoardNumber(36), new BoardNumber(70), new BoardNumber(42), new BoardNumber(21),
 new BoardNumber(3), new BoardNumber(16), new BoardNumber(49), new BoardNumber(79), new BoardNumber(98),
 new BoardNumber(74), new BoardNumber(25), new BoardNumber(8), new BoardNumber(84), new BoardNumber(19),
 new BoardNumber(61), new BoardNumber(80), new BoardNumber(47), new BoardNumber(65), new BoardNumber(64),
 new BoardNumber(91), new BoardNumber(62), new BoardNumber(52), new BoardNumber(9), new BoardNumber(40)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(1), new BoardNumber(85), new BoardNumber(63), new BoardNumber(7), new BoardNumber(2),
 new BoardNumber(0), new BoardNumber(20), new BoardNumber(61), new BoardNumber(26), new BoardNumber(77),
 new BoardNumber(99), new BoardNumber(37), new BoardNumber(74), new BoardNumber(42), new BoardNumber(76),
 new BoardNumber(25), new BoardNumber(94), new BoardNumber(19), new BoardNumber(78), new BoardNumber(60),
 new BoardNumber(79), new BoardNumber(72), new BoardNumber(95), new BoardNumber(22), new BoardNumber(11)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(51), new BoardNumber(21), new BoardNumber(79), new BoardNumber(76), new BoardNumber(32),
 new BoardNumber(55), new BoardNumber(23), new BoardNumber(69), new BoardNumber(19), new BoardNumber(61),
 new BoardNumber(71), new BoardNumber(54), new BoardNumber(94), new BoardNumber(47), new BoardNumber(92),
 new BoardNumber(5), new BoardNumber(64), new BoardNumber(6), new BoardNumber(68), new BoardNumber(16),
 new BoardNumber(91), new BoardNumber(81), new BoardNumber(9), new BoardNumber(99), new BoardNumber(30)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(61), new BoardNumber(69), new BoardNumber(82), new BoardNumber(86), new BoardNumber(68),
 new BoardNumber(66), new BoardNumber(81), new BoardNumber(28), new BoardNumber(38), new BoardNumber(36),
 new BoardNumber(26), new BoardNumber(29), new BoardNumber(31), new BoardNumber(11), new BoardNumber(8),
 new BoardNumber(72), new BoardNumber(51), new BoardNumber(12), new BoardNumber(95), new BoardNumber(63),
 new BoardNumber(18), new BoardNumber(30), new BoardNumber(88), new BoardNumber(17), new BoardNumber(32)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(34), new BoardNumber(8), new BoardNumber(14), new BoardNumber(42), new BoardNumber(67),
 new BoardNumber(66), new BoardNumber(79), new BoardNumber(65), new BoardNumber(20), new BoardNumber(52),
 new BoardNumber(37), new BoardNumber(87), new BoardNumber(74), new BoardNumber(24), new BoardNumber(3),
 new BoardNumber(59), new BoardNumber(54), new BoardNumber(21), new BoardNumber(32), new BoardNumber(89),
 new BoardNumber(31), new BoardNumber(4), new BoardNumber(62), new BoardNumber(76), new BoardNumber(30)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(11), new BoardNumber(93), new BoardNumber(8), new BoardNumber(92), new BoardNumber(55),
 new BoardNumber(38), new BoardNumber(72), new BoardNumber(99), new BoardNumber(3), new BoardNumber(83),
 new BoardNumber(12), new BoardNumber(75), new BoardNumber(0), new BoardNumber(41), new BoardNumber(46),
 new BoardNumber(17), new BoardNumber(25), new BoardNumber(5), new BoardNumber(39), new BoardNumber(48),
 new BoardNumber(14), new BoardNumber(18), new BoardNumber(86), new BoardNumber(29), new BoardNumber(84)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(6), new BoardNumber(20), new BoardNumber(41), new BoardNumber(51), new BoardNumber(48),
 new BoardNumber(5), new BoardNumber(67), new BoardNumber(30), new BoardNumber(24), new BoardNumber(47),
 new BoardNumber(3), new BoardNumber(8), new BoardNumber(92), new BoardNumber(22), new BoardNumber(39),
 new BoardNumber(4), new BoardNumber(56), new BoardNumber(36), new BoardNumber(31), new BoardNumber(75),
 new BoardNumber(2), new BoardNumber(45), new BoardNumber(85), new BoardNumber(81), new BoardNumber(96)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(47), new BoardNumber(43), new BoardNumber(72), new BoardNumber(22), new BoardNumber(3),
 new BoardNumber(19), new BoardNumber(87), new BoardNumber(53), new BoardNumber(12), new BoardNumber(60),
 new BoardNumber(29), new BoardNumber(40), new BoardNumber(56), new BoardNumber(68), new BoardNumber(18),
 new BoardNumber(66), new BoardNumber(97), new BoardNumber(70), new BoardNumber(33), new BoardNumber(39),
 new BoardNumber(85), new BoardNumber(37), new BoardNumber(0), new BoardNumber(90), new BoardNumber(98)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(61), new BoardNumber(35), new BoardNumber(81), new BoardNumber(84), new BoardNumber(94),
 new BoardNumber(11), new BoardNumber(1), new BoardNumber(58), new BoardNumber(45), new BoardNumber(77),
 new BoardNumber(6), new BoardNumber(99), new BoardNumber(67), new BoardNumber(36), new BoardNumber(43),
 new BoardNumber(5), new BoardNumber(7), new BoardNumber(0), new BoardNumber(87), new BoardNumber(80),
 new BoardNumber(44), new BoardNumber(78), new BoardNumber(39), new BoardNumber(70), new BoardNumber(20)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(58), new BoardNumber(34), new BoardNumber(49), new BoardNumber(29), new BoardNumber(75),
 new BoardNumber(17), new BoardNumber(15), new BoardNumber(28), new BoardNumber(23), new BoardNumber(84),
 new BoardNumber(59), new BoardNumber(25), new BoardNumber(92), new BoardNumber(48), new BoardNumber(0),
 new BoardNumber(20), new BoardNumber(81), new BoardNumber(47), new BoardNumber(3), new BoardNumber(71),
 new BoardNumber(68), new BoardNumber(60), new BoardNumber(5), new BoardNumber(22), new BoardNumber(87)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(90), new BoardNumber(32), new BoardNumber(41), new BoardNumber(39), new BoardNumber(6),
 new BoardNumber(36), new BoardNumber(78), new BoardNumber(67), new BoardNumber(24), new BoardNumber(50),
 new BoardNumber(55), new BoardNumber(72), new BoardNumber(52), new BoardNumber(75), new BoardNumber(44),
 new BoardNumber(87), new BoardNumber(15), new BoardNumber(92), new BoardNumber(31), new BoardNumber(58),
 new BoardNumber(83), new BoardNumber(89), new BoardNumber(68), new BoardNumber(19), new BoardNumber(43)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(99), new BoardNumber(44), new BoardNumber(53), new BoardNumber(68), new BoardNumber(25),
 new BoardNumber(71), new BoardNumber(67), new BoardNumber(16), new BoardNumber(19), new BoardNumber(36),
 new BoardNumber(35), new BoardNumber(58), new BoardNumber(14), new BoardNumber(86), new BoardNumber(48),
 new BoardNumber(88), new BoardNumber(18), new BoardNumber(61), new BoardNumber(24), new BoardNumber(23),
 new BoardNumber(87), new BoardNumber(9), new BoardNumber(91), new BoardNumber(37), new BoardNumber(15)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(37), new BoardNumber(5), new BoardNumber(63), new BoardNumber(68), new BoardNumber(28),
 new BoardNumber(41), new BoardNumber(50), new BoardNumber(76), new BoardNumber(99), new BoardNumber(64),
 new BoardNumber(34), new BoardNumber(92), new BoardNumber(78), new BoardNumber(94), new BoardNumber(71),
 new BoardNumber(11), new BoardNumber(96), new BoardNumber(97), new BoardNumber(42), new BoardNumber(58),
 new BoardNumber(33), new BoardNumber(45), new BoardNumber(0), new BoardNumber(93), new BoardNumber(48)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(33), new BoardNumber(68), new BoardNumber(9), new BoardNumber(12), new BoardNumber(81),
 new BoardNumber(60), new BoardNumber(98), new BoardNumber(28), new BoardNumber(8), new BoardNumber(99),
 new BoardNumber(14), new BoardNumber(17), new BoardNumber(6), new BoardNumber(82), new BoardNumber(15),
 new BoardNumber(57), new BoardNumber(69), new BoardNumber(43), new BoardNumber(38), new BoardNumber(29),
 new BoardNumber(47), new BoardNumber(84), new BoardNumber(76), new BoardNumber(22), new BoardNumber(18)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(79), new BoardNumber(70), new BoardNumber(92), new BoardNumber(38), new BoardNumber(47),
 new BoardNumber(12), new BoardNumber(82), new BoardNumber(98), new BoardNumber(46), new BoardNumber(0),
 new BoardNumber(76), new BoardNumber(15), new BoardNumber(53), new BoardNumber(59), new BoardNumber(97),
 new BoardNumber(18), new BoardNumber(52), new BoardNumber(49), new BoardNumber(29), new BoardNumber(96),
 new BoardNumber(44), new BoardNumber(64), new BoardNumber(68), new BoardNumber(89), new BoardNumber(24)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(95), new BoardNumber(14), new BoardNumber(17), new BoardNumber(27), new BoardNumber(42),
 new BoardNumber(55), new BoardNumber(43), new BoardNumber(57), new BoardNumber(29), new BoardNumber(25),
 new BoardNumber(34), new BoardNumber(73), new BoardNumber(86), new BoardNumber(50), new BoardNumber(16),
 new BoardNumber(69), new BoardNumber(37), new BoardNumber(75), new BoardNumber(63), new BoardNumber(39),
 new BoardNumber(78), new BoardNumber(79), new BoardNumber(3), new BoardNumber(4), new BoardNumber(30)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(27), new BoardNumber(31), new BoardNumber(15), new BoardNumber(92), new BoardNumber(46),
 new BoardNumber(36), new BoardNumber(23), new BoardNumber(72), new BoardNumber(40), new BoardNumber(50),
 new BoardNumber(51), new BoardNumber(99), new BoardNumber(55), new BoardNumber(89), new BoardNumber(21),
 new BoardNumber(12), new BoardNumber(70), new BoardNumber(84), new BoardNumber(63), new BoardNumber(85),
 new BoardNumber(78), new BoardNumber(88), new BoardNumber(77), new BoardNumber(75), new BoardNumber(0)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(15), new BoardNumber(67), new BoardNumber(40), new BoardNumber(39), new BoardNumber(28),
 new BoardNumber(9), new BoardNumber(79), new BoardNumber(22), new BoardNumber(52), new BoardNumber(75),
 new BoardNumber(96), new BoardNumber(65), new BoardNumber(86), new BoardNumber(98), new BoardNumber(14),
 new BoardNumber(97), new BoardNumber(87), new BoardNumber(44), new BoardNumber(84), new BoardNumber(68),
 new BoardNumber(36), new BoardNumber(26), new BoardNumber(89), new BoardNumber(43), new BoardNumber(27)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(79), new BoardNumber(59), new BoardNumber(48), new BoardNumber(27), new BoardNumber(36),
 new BoardNumber(85), new BoardNumber(92), new BoardNumber(93), new BoardNumber(76), new BoardNumber(24),
 new BoardNumber(2), new BoardNumber(25), new BoardNumber(7), new BoardNumber(42), new BoardNumber(90),
 new BoardNumber(23), new BoardNumber(29), new BoardNumber(74), new BoardNumber(35), new BoardNumber(86),
 new BoardNumber(58), new BoardNumber(60), new BoardNumber(31), new BoardNumber(75), new BoardNumber(57)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(10), new BoardNumber(43), new BoardNumber(83), new BoardNumber(75), new BoardNumber(8),
 new BoardNumber(88), new BoardNumber(12), new BoardNumber(38), new BoardNumber(30), new BoardNumber(9),
 new BoardNumber(60), new BoardNumber(67), new BoardNumber(59), new BoardNumber(76), new BoardNumber(6),
 new BoardNumber(55), new BoardNumber(45), new BoardNumber(74), new BoardNumber(34), new BoardNumber(25),
 new BoardNumber(97), new BoardNumber(49), new BoardNumber(65), new BoardNumber(96), new BoardNumber(69)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(59), new BoardNumber(86), new BoardNumber(15), new BoardNumber(3), new BoardNumber(19),
 new BoardNumber(89), new BoardNumber(4), new BoardNumber(74), new BoardNumber(61), new BoardNumber(23),
 new BoardNumber(52), new BoardNumber(98), new BoardNumber(8), new BoardNumber(79), new BoardNumber(39),
 new BoardNumber(95), new BoardNumber(17), new BoardNumber(22), new BoardNumber(14), new BoardNumber(51),
 new BoardNumber(50), new BoardNumber(18), new BoardNumber(94), new BoardNumber(30), new BoardNumber(84)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(19), new BoardNumber(63), new BoardNumber(58), new BoardNumber(72), new BoardNumber(67),
 new BoardNumber(35), new BoardNumber(93), new BoardNumber(29), new BoardNumber(91), new BoardNumber(0),
 new BoardNumber(39), new BoardNumber(26), new BoardNumber(43), new BoardNumber(84), new BoardNumber(21),
 new BoardNumber(70), new BoardNumber(42), new BoardNumber(2), new BoardNumber(53), new BoardNumber(12),
 new BoardNumber(59), new BoardNumber(99), new BoardNumber(8), new BoardNumber(1), new BoardNumber(86)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(23), new BoardNumber(86), new BoardNumber(34), new BoardNumber(22), new BoardNumber(65),
 new BoardNumber(71), new BoardNumber(10), new BoardNumber(16), new BoardNumber(50), new BoardNumber(91),
 new BoardNumber(66), new BoardNumber(89), new BoardNumber(49), new BoardNumber(81), new BoardNumber(43),
 new BoardNumber(40), new BoardNumber(7), new BoardNumber(26), new BoardNumber(75), new BoardNumber(61),
 new BoardNumber(62), new BoardNumber(59), new BoardNumber(2), new BoardNumber(46), new BoardNumber(95)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(24), new BoardNumber(21), new BoardNumber(0), new BoardNumber(49), new BoardNumber(25),
 new BoardNumber(92), new BoardNumber(42), new BoardNumber(48), new BoardNumber(12), new BoardNumber(7),
 new BoardNumber(81), new BoardNumber(93), new BoardNumber(59), new BoardNumber(68), new BoardNumber(3),
 new BoardNumber(14), new BoardNumber(23), new BoardNumber(63), new BoardNumber(39), new BoardNumber(29),
 new BoardNumber(35), new BoardNumber(43), new BoardNumber(6), new BoardNumber(44), new BoardNumber(89)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(67), new BoardNumber(74), new BoardNumber(95), new BoardNumber(34), new BoardNumber(10),
 new BoardNumber(39), new BoardNumber(90), new BoardNumber(59), new BoardNumber(44), new BoardNumber(51),
 new BoardNumber(17), new BoardNumber(16), new BoardNumber(97), new BoardNumber(24), new BoardNumber(62),
 new BoardNumber(20), new BoardNumber(54), new BoardNumber(76), new BoardNumber(63), new BoardNumber(88),
 new BoardNumber(87), new BoardNumber(66), new BoardNumber(14), new BoardNumber(78), new BoardNumber(82)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(96), new BoardNumber(86), new BoardNumber(67), new BoardNumber(59), new BoardNumber(79),
 new BoardNumber(66), new BoardNumber(3), new BoardNumber(30), new BoardNumber(77), new BoardNumber(71),
 new BoardNumber(2), new BoardNumber(91), new BoardNumber(99), new BoardNumber(82), new BoardNumber(31),
 new BoardNumber(48), new BoardNumber(65), new BoardNumber(75), new BoardNumber(98), new BoardNumber(53),
 new BoardNumber(63), new BoardNumber(54), new BoardNumber(64), new BoardNumber(76), new BoardNumber(1)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(85), new BoardNumber(96), new BoardNumber(40), new BoardNumber(98), new BoardNumber(24),
 new BoardNumber(16), new BoardNumber(20), new BoardNumber(10), new BoardNumber(23), new BoardNumber(17),
 new BoardNumber(79), new BoardNumber(59), new BoardNumber(53), new BoardNumber(42), new BoardNumber(65),
 new BoardNumber(67), new BoardNumber(2), new BoardNumber(5), new BoardNumber(80), new BoardNumber(75),
 new BoardNumber(62), new BoardNumber(38), new BoardNumber(19), new BoardNumber(74), new BoardNumber(73)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(43), new BoardNumber(10), new BoardNumber(79), new BoardNumber(92), new BoardNumber(8),
 new BoardNumber(52), new BoardNumber(36), new BoardNumber(4), new BoardNumber(5), new BoardNumber(67),
 new BoardNumber(56), new BoardNumber(29), new BoardNumber(33), new BoardNumber(24), new BoardNumber(97),
 new BoardNumber(85), new BoardNumber(17), new BoardNumber(53), new BoardNumber(75), new BoardNumber(65),
 new BoardNumber(62), new BoardNumber(64), new BoardNumber(1), new BoardNumber(21), new BoardNumber(83)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(93), new BoardNumber(92), new BoardNumber(79), new BoardNumber(17), new BoardNumber(12),
 new BoardNumber(40), new BoardNumber(88), new BoardNumber(6), new BoardNumber(82), new BoardNumber(34),
 new BoardNumber(90), new BoardNumber(96), new BoardNumber(53), new BoardNumber(25), new BoardNumber(43),
 new BoardNumber(14), new BoardNumber(62), new BoardNumber(54), new BoardNumber(10), new BoardNumber(39),
 new BoardNumber(49), new BoardNumber(68), new BoardNumber(41), new BoardNumber(16), new BoardNumber(44)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(67), new BoardNumber(99), new BoardNumber(24), new BoardNumber(58), new BoardNumber(76),
 new BoardNumber(43), new BoardNumber(53), new BoardNumber(59), new BoardNumber(54), new BoardNumber(51),
 new BoardNumber(47), new BoardNumber(6), new BoardNumber(61), new BoardNumber(8), new BoardNumber(2),
 new BoardNumber(80), new BoardNumber(68), new BoardNumber(90), new BoardNumber(14), new BoardNumber(4),
 new BoardNumber(29), new BoardNumber(46), new BoardNumber(94), new BoardNumber(89), new BoardNumber(50)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(14), new BoardNumber(45), new BoardNumber(19), new BoardNumber(33), new BoardNumber(43),
 new BoardNumber(6), new BoardNumber(55), new BoardNumber(4), new BoardNumber(31), new BoardNumber(80),
 new BoardNumber(51), new BoardNumber(2), new BoardNumber(69), new BoardNumber(68), new BoardNumber(61),
 new BoardNumber(71), new BoardNumber(70), new BoardNumber(79), new BoardNumber(91), new BoardNumber(93),
 new BoardNumber(66), new BoardNumber(18), new BoardNumber(54), new BoardNumber(13), new BoardNumber(87)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(8), new BoardNumber(45), new BoardNumber(61), new BoardNumber(54), new BoardNumber(30),
 new BoardNumber(85), new BoardNumber(16), new BoardNumber(19), new BoardNumber(82), new BoardNumber(37),
 new BoardNumber(56), new BoardNumber(39), new BoardNumber(11), new BoardNumber(47), new BoardNumber(4),
 new BoardNumber(74), new BoardNumber(70), new BoardNumber(10), new BoardNumber(60), new BoardNumber(91),
 new BoardNumber(21), new BoardNumber(63), new BoardNumber(95), new BoardNumber(53), new BoardNumber(72)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(71), new BoardNumber(21), new BoardNumber(63), new BoardNumber(86), new BoardNumber(27),
 new BoardNumber(53), new BoardNumber(52), new BoardNumber(40), new BoardNumber(23), new BoardNumber(81),
 new BoardNumber(2), new BoardNumber(47), new BoardNumber(92), new BoardNumber(68), new BoardNumber(15),
 new BoardNumber(46), new BoardNumber(45), new BoardNumber(31), new BoardNumber(8), new BoardNumber(1),
 new BoardNumber(34), new BoardNumber(80), new BoardNumber(37), new BoardNumber(11), new BoardNumber(69)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(96), new BoardNumber(0), new BoardNumber(15), new BoardNumber(90), new BoardNumber(66),
 new BoardNumber(65), new BoardNumber(43), new BoardNumber(92), new BoardNumber(83), new BoardNumber(18),
 new BoardNumber(3), new BoardNumber(47), new BoardNumber(19), new BoardNumber(8), new BoardNumber(32),
 new BoardNumber(71), new BoardNumber(26), new BoardNumber(42), new BoardNumber(34), new BoardNumber(28),
 new BoardNumber(62), new BoardNumber(99), new BoardNumber(55), new BoardNumber(5), new BoardNumber(12)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(37), new BoardNumber(99), new BoardNumber(30), new BoardNumber(21), new BoardNumber(3),
 new BoardNumber(63), new BoardNumber(18), new BoardNumber(68), new BoardNumber(47), new BoardNumber(27),
 new BoardNumber(57), new BoardNumber(0), new BoardNumber(65), new BoardNumber(85), new BoardNumber(20),
 new BoardNumber(7), new BoardNumber(58), new BoardNumber(40), new BoardNumber(92), new BoardNumber(43),
 new BoardNumber(15), new BoardNumber(19), new BoardNumber(5), new BoardNumber(4), new BoardNumber(53)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(46), new BoardNumber(16), new BoardNumber(45), new BoardNumber(95), new BoardNumber(68),
 new BoardNumber(6), new BoardNumber(44), new BoardNumber(31), new BoardNumber(47), new BoardNumber(73),
 new BoardNumber(84), new BoardNumber(82), new BoardNumber(71), new BoardNumber(75), new BoardNumber(94),
 new BoardNumber(26), new BoardNumber(25), new BoardNumber(17), new BoardNumber(32), new BoardNumber(49),
 new BoardNumber(18), new BoardNumber(96), new BoardNumber(13), new BoardNumber(58), new BoardNumber(9)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(71), new BoardNumber(36), new BoardNumber(13), new BoardNumber(68), new BoardNumber(10),
 new BoardNumber(84), new BoardNumber(7), new BoardNumber(60), new BoardNumber(79), new BoardNumber(41),
 new BoardNumber(1), new BoardNumber(83), new BoardNumber(43), new BoardNumber(81), new BoardNumber(97),
 new BoardNumber(90), new BoardNumber(53), new BoardNumber(80), new BoardNumber(19), new BoardNumber(38),
 new BoardNumber(48), new BoardNumber(25), new BoardNumber(32), new BoardNumber(42), new BoardNumber(29)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(37), new BoardNumber(68), new BoardNumber(86), new BoardNumber(44), new BoardNumber(78),
 new BoardNumber(87), new BoardNumber(67), new BoardNumber(77), new BoardNumber(70), new BoardNumber(60),
 new BoardNumber(45), new BoardNumber(34), new BoardNumber(27), new BoardNumber(15), new BoardNumber(47),
 new BoardNumber(12), new BoardNumber(21), new BoardNumber(13), new BoardNumber(55), new BoardNumber(26),
 new BoardNumber(81), new BoardNumber(41), new BoardNumber(63), new BoardNumber(40), new BoardNumber(74)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(24), new BoardNumber(50), new BoardNumber(93), new BoardNumber(94), new BoardNumber(57),
 new BoardNumber(99), new BoardNumber(4), new BoardNumber(56), new BoardNumber(5), new BoardNumber(28),
 new BoardNumber(42), new BoardNumber(31), new BoardNumber(22), new BoardNumber(6), new BoardNumber(76),
 new BoardNumber(90), new BoardNumber(89), new BoardNumber(16), new BoardNumber(49), new BoardNumber(59),
 new BoardNumber(9), new BoardNumber(7), new BoardNumber(43), new BoardNumber(71), new BoardNumber(54)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(69), new BoardNumber(75), new BoardNumber(94), new BoardNumber(38), new BoardNumber(46),
 new BoardNumber(52), new BoardNumber(64), new BoardNumber(50), new BoardNumber(72), new BoardNumber(42),
 new BoardNumber(76), new BoardNumber(63), new BoardNumber(13), new BoardNumber(60), new BoardNumber(10),
 new BoardNumber(99), new BoardNumber(80), new BoardNumber(43), new BoardNumber(33), new BoardNumber(17),
 new BoardNumber(25), new BoardNumber(31), new BoardNumber(4), new BoardNumber(89), new BoardNumber(22)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(88), new BoardNumber(57), new BoardNumber(22), new BoardNumber(66), new BoardNumber(34),
 new BoardNumber(85), new BoardNumber(16), new BoardNumber(87), new BoardNumber(95), new BoardNumber(59),
 new BoardNumber(73), new BoardNumber(2), new BoardNumber(46), new BoardNumber(5), new BoardNumber(29),
 new BoardNumber(25), new BoardNumber(69), new BoardNumber(53), new BoardNumber(6), new BoardNumber(14),
 new BoardNumber(96), new BoardNumber(77), new BoardNumber(19), new BoardNumber(91), new BoardNumber(43)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(46), new BoardNumber(99), new BoardNumber(52), new BoardNumber(47), new BoardNumber(76),
 new BoardNumber(89), new BoardNumber(53), new BoardNumber(24), new BoardNumber(13), new BoardNumber(59),
 new BoardNumber(45), new BoardNumber(5), new BoardNumber(1), new BoardNumber(30), new BoardNumber(19),
 new BoardNumber(68), new BoardNumber(25), new BoardNumber(22), new BoardNumber(10), new BoardNumber(73),
 new BoardNumber(42), new BoardNumber(27), new BoardNumber(31), new BoardNumber(0), new BoardNumber(94)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(42), new BoardNumber(44), new BoardNumber(98), new BoardNumber(89), new BoardNumber(87),
 new BoardNumber(65), new BoardNumber(10), new BoardNumber(80), new BoardNumber(56), new BoardNumber(41),
 new BoardNumber(3), new BoardNumber(35), new BoardNumber(95), new BoardNumber(48), new BoardNumber(43),
 new BoardNumber(85), new BoardNumber(97), new BoardNumber(83), new BoardNumber(12), new BoardNumber(94),
 new BoardNumber(50), new BoardNumber(38), new BoardNumber(93), new BoardNumber(47), new BoardNumber(17)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(16), new BoardNumber(73), new BoardNumber(18), new BoardNumber(81), new BoardNumber(89),
 new BoardNumber(6), new BoardNumber(48), new BoardNumber(54), new BoardNumber(93), new BoardNumber(19),
 new BoardNumber(35), new BoardNumber(52), new BoardNumber(88), new BoardNumber(49), new BoardNumber(31),
 new BoardNumber(43), new BoardNumber(79), new BoardNumber(83), new BoardNumber(14), new BoardNumber(28),
 new BoardNumber(50), new BoardNumber(62), new BoardNumber(98), new BoardNumber(26), new BoardNumber(22)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(38), new BoardNumber(47), new BoardNumber(7), new BoardNumber(20), new BoardNumber(35),
 new BoardNumber(45), new BoardNumber(76), new BoardNumber(63), new BoardNumber(96), new BoardNumber(24),
 new BoardNumber(98), new BoardNumber(53), new BoardNumber(2), new BoardNumber(87), new BoardNumber(80),
 new BoardNumber(83), new BoardNumber(86), new BoardNumber(92), new BoardNumber(48), new BoardNumber(1),
 new BoardNumber(73), new BoardNumber(60), new BoardNumber(26), new BoardNumber(94), new BoardNumber(6)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(80), new BoardNumber(50), new BoardNumber(29), new BoardNumber(53), new BoardNumber(92),
 new BoardNumber(66), new BoardNumber(90), new BoardNumber(79), new BoardNumber(98), new BoardNumber(46),
 new BoardNumber(40), new BoardNumber(21), new BoardNumber(58), new BoardNumber(38), new BoardNumber(60),
 new BoardNumber(35), new BoardNumber(13), new BoardNumber(72), new BoardNumber(28), new BoardNumber(6),
 new BoardNumber(48), new BoardNumber(76), new BoardNumber(51), new BoardNumber(96), new BoardNumber(12)}));
boards.Add(new Board(new BoardNumber[25]{
 new BoardNumber(79), new BoardNumber(80), new BoardNumber(24), new BoardNumber(37), new BoardNumber(51),
 new BoardNumber(86), new BoardNumber(70), new BoardNumber(1), new BoardNumber(22), new BoardNumber(71),
 new BoardNumber(52), new BoardNumber(69), new BoardNumber(10), new BoardNumber(83), new BoardNumber(13),
 new BoardNumber(12), new BoardNumber(40), new BoardNumber(3), new BoardNumber(0), new BoardNumber(30),
 new BoardNumber(46), new BoardNumber(50), new BoardNumber(48), new BoardNumber(76), new BoardNumber(5)}));

#endregion

int result = BoardUtility.GetLastScore(boards, extraction);

Console.WriteLine("The result is: " + result);
