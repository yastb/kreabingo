﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bingo.Classes
{
    //this is the class for the entire bingo board
    public class Board
    {
        public BoardNumber[][] grid { get; set; }
        public bool Winner { get; set; } = false;

        public Board(BoardNumber[] numbers)
        {
            if(numbers == null || numbers.Length != 25)
            {
                throw new Exception("A grid must contain 25 numbers");
            }
            this.grid = new BoardNumber[5][];
            this.grid[0] = new BoardNumber[5];
            this.grid[1] = new BoardNumber[5];
            this.grid[2] = new BoardNumber[5];
            this.grid[3] = new BoardNumber[5];
            this.grid[4] = new BoardNumber[5];
            for (int i = 0; i < 5; i++)
            {
                this.grid[i][0] = numbers[i * 5];
                this.grid[i][1] = numbers[i * 5 + 1];
                this.grid[i][2] = numbers[i * 5 + 2];
                this.grid[i][3] = numbers[i * 5 + 3];
                this.grid[i][4] = numbers[i * 5 + 4];
            }
        }

        private bool checkRow(int i)
        {
            return this.grid[i][0].Extracted && this.grid[i][1].Extracted && this.grid[i][2].Extracted && 
                this.grid[i][3].Extracted && this.grid[i][4].Extracted;
        }

        private bool checkColumn(int i)
        {
            return this.grid[0][i].Extracted && this.grid[1][i].Extracted && this.grid[2][i].Extracted &&
                this.grid[3][i].Extracted && this.grid[4][i].Extracted;
        }

        //set the extracted number and check if the board is a winner one
        public void SetExtracted(int number)
        {
            for (int i = 0; i < 5; i++)
            {
                for(int j=0;j<5; j++)
                {
                    if(this.grid[i][j].Number == number)
                    {
                        this.grid[i][j].Extracted = true;
                    }
                }
            }
            for (int i = 0; i < 5 && !this.Winner; i++)
            {
                this.Winner = this.Winner || checkColumn(i);
                this.Winner = this.Winner || checkRow(i);
            }
        }

        //calculates the score of the winning board
        public int CalculateScore(int lastExtract)
        {
            int score = 0;
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    if (!this.grid[i][j].Extracted)
                    {
                        score+= this.grid[i][j].Number;
                    }
                }
            }
            return score * lastExtract;
        }

        //draw the board on the console
        public void Print()
        {
            for(int i = 0; i < 5; i++)
            {
                for(int j = 0; j < 5; j++)
                {
                    Console.Write(" ");
                    Console.BackgroundColor = this.grid[i][j].Extracted ? ConsoleColor.Red : ConsoleColor.Black;
                    Console.Write(this.grid[i][j].Number > 9 ? (this.grid[i][j].Number.ToString()) : (" "+this.grid[i][j].Number.ToString()));
                    Console.BackgroundColor = ConsoleColor.Black;
                }
                Console.Write("\n");
            }
        }
    }
}
