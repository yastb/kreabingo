﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bingo.Classes
{
    //This is class for the single cell of the bingo.
    public class BoardNumber
    {
        public int Number { get; set; }
        public bool Extracted { get; set; } = false;

        public BoardNumber(int number)
        {
            this.Number = number;
        }
    }
}
