﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bingo.Classes
{
    public class BoardUtility
    {
        public static int GetLastScore(List<Board> boards, List<int> extraction)
        {
            int lastScore = -1;

            int numberOfWinningBoards = 0;
            int numberOfBoards = boards.Count();

            foreach (int n in extraction)
            {
                foreach (Board board in boards)
                {
                    if (!board.Winner)
                    {
                        board.SetExtracted(n);
                        if (board.Winner)
                        {
                            numberOfWinningBoards++;
                            //I need just the last score, no need to calculate scores of previous winning boards
                            if (numberOfWinningBoards == numberOfBoards)
                            {
                                lastScore = board.CalculateScore(n);
                            }
                        }
                    }
                }
                //I remove the winning boards, no need to set extracted numbers to them
                boards = boards.Where(x => !x.Winner).ToList();
                if (boards.Count == 0)
                {
                    break;
                }
            }
            return lastScore;
        }
    }
}
